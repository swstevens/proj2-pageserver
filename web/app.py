from flask import Flask, render_template, abort, request

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route('/<name>')
def page_search(name):
    try:
        return render_template(name), 200
    except:
        abort(404)

@app.errorhandler(404)
def page_not_found(e):
    full_url = request.full_path
    if ".." in full_url or "//" in full_url or "~" in full_url:
        return render_template('403.html'), 403
        # return full_url
        # abort(403)
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
